document.addEventListener("DOMContentLoaded", () => {
    const url = new URL(window.location.href).host;
    const script = document.createElement("script");
    script.id = "plausible-script";
    script.async = true;
    script.defer = true;
    script.src = "https://plausible.io/js/script.js";
    script.setAttribute("data-domain", url);
    document.head.appendChild(script);
});
