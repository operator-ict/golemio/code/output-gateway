# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [3.13.6] - 2025-03-04

-   No changelog

## [3.13.5] - 2025-03-03

-   No changelog

## [3.13.4] - 2025-02-25

-   No changelog

## [3.13.3] - 2025-02-20

-   No changelog

## [3.13.2] - 2025-02-13

### Changed

-   ci/cd postgres image reference ([core#125](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/125))

## [3.13.1] - 2025-02-11

-   No changelog

## [3.13.0] - 2025-01-20

### Changed

-   Change TS build target from ES2015 to ES2021 ([core#121](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/121))

## [3.12.1] - 2025-01-07

-   No changelog

## [3.12.0] - 2024-12-17

### Changed

-   Disable Sentry Express middleware ([p0255#90](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/90))

### Fixed

-   Memory leaks related to Sentry Express middleware ([p0255#90](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/90))

### Changed

-   Upgrade Node.js to v20.18.0 ([core#118](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/118))
-   handle Error 406 as warning ([#226](https://gitlab.com/operator-ict/golemio/code/output-gateway/-/issues/226))

## [3.11.0] - 2024-12-12

### Removed

-   remove exporting module ([general#625](https://gitlab.com/operator-ict/golemio/code/general/-/issues/625))

## [3.10.30] - 2024-12-10

-   No changelog

## [3.10.29] - 2024-11-28

-   No changelog

## [3.10.28] - 2024-11-27

### Added

-   Plausible for tracking user behavior in "/docs/openapi" and "/docs/public-openapi" ([golemio#631](https://gitlab.com/operator-ict/golemio/code/general/-/issues/631))

## [3.10.27] - 2024-11-26

-   No changelog

## [3.10.26] - 2024-11-14

-   No changelog

## [3.10.25] - 2024-11-05

-   No changelog

## [3.10.24] - 2024-10-15

-   No changelog

## [3.10.23] - 2024-09-26

-   No changelog

## [3.10.22] - 2024-09-24

-   No changelog

## [3.10.21] - 2024-09-12

-   No changelog

## [3.10.20] - 2024-09-10

-   No changelog

## [3.10.19] - 2024-09-09

### Changed

-   Replace node-redis with ioredis ([core#39](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/39))

## [3.10.18] - 2024-09-05

### Added

-   gzip encoding to openapi ([pid#413](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/413))

## [3.10.17] - 2024-08-29

### Added

-   `/v2/vehiclesharing/mvts` router ([p0131#175](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/175))

## [3.10.16] - 2024-08-27

-   No changelog

## [3.10.15] - 2024-08-20

### Removed

-   remove CacheMiddleware ([core#110](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/110))

## [3.10.14] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [3.10.13] - 2024-08-13

-   No changelog

## [3.10.12] - 2024-08-06

-   No changelog

## [3.10.11] - 2024-08-01

-   No changelog

## [3.10.10] - 2024-07-30

-   No changelog

## [3.10.9] - 2024-07-23

-   No changelog

## [3.10.8] - 2024-07-23

-   No changelog

## [3.10.7] - 2024-07-23

-   No changelog

## [3.10.6] - 2024-07-17

-   No changelog

## [3.10.5] - 2024-07-17

-   No changelog

## [3.10.4] - 2024-07-15

-   No changelog

## [3.10.3] - 2024-07-11

-   waste-collection module rolled back to 1.3.18

## [3.10.2] - 2024-07-10

-   No changelog

## [3.10.1] - 2024-07-04

-   No changelog

## [3.10.0] - 2024-06-24

-   No changelog

## [3.9.19] - 2024-06-19

-   No changelog

## [3.9.18] - 2024-06-17

### Added

-   Adding internal EPs to openapi docs filter ([parkings#14](https://gitlab.com/operator-ict/golemio/projekty/oict/parkings/-/issues/14))

## [3.9.17] - 2024-06-05

-   No changelog

## [3.9.16] - 2024-06-03

### Added

-   Energetics module API ([prevzeti-ed#20](https://gitlab.com/operator-ict/golemio/projekty/energetika/p0262.prevzeti-energeticke-databaze/-/issues/20))

## [3.9.15] - 2024-05-27

-   No changelog

## [3.9.14] - 2024-05-22

-   No changelog

## [3.9.13] - 2024-05-30

-   No changelog

## [3.9.12] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [3.9.11] - 2024-04-29

-   No changelog

## [3.9.10] - 2024-04-24

-   No changelog

## [3.9.9] - 2024-04-15

### Added

-   Expose static openapi docs files

## [3.9.8] - 2024-04-10

-   No changelog

## [3.9.7] - 2024-04-08

-   No changelog

## [3.9.6] - 2024-04-05

-   No changelog

## [3.9.5] - 2024-04-03

-   No changelog

## [3.9.4] - 2024-03-27

-   No changelog

## [3.9.3] - 2024-03-25

-   No changelog

## [3.9.2] - 2024-03-20

-   No changelog

## [3.9.1] - 2024-03-20

-   No changelog

## [3.9.0] - 2024-03-04

-   No changelog

## [3.8.13] - 2024-02-28

-   No changelog

## [3.8.12] - 2024-02-26

-   No changelog

## [3.8.11] - 2024-02-21

-   No changelog

## [3.8.10] - 2024-02-19

-   No changelog

## [3.8.9] - 2024-02-14

-   No changelog

## [3.8.8] - 2024-02-02

-   No changelog

## [3.8.7] - 2024-01-29

-   No changelog

## [3.8.6] - 2024-01-22

-   No changelog

## [3.8.5] - 2024-01-17

### Changed

-   Improve README.md
-   Update Swagger UI, improve OpenAPI docs

### Removed

-   Apiary docs (permanently moved to OpenAPI) ([general#498](https://gitlab.com/operator-ict/golemio/code/general/-/issues/498))
-   Dredd tests

## [3.8.4] - 2024-01-15

-   No changelog

## [3.8.3] - 2024-01-04

-   No changelog

## [3.8.2] - 2023-12-20

### Fixed

-   Improve request aborted handling ([core#86](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/86))

## [3.8.1] - 2023-12-13

-   No changelog

## [3.8.0] - 2023-12-04

-   No changelog

## [3.7.13] - 2023-11-29

### Added

-   new API for waze-ccp ~ potholes ([potholes#11](https://gitlab.com/operator-ict/golemio/projekty/tsk/potholes/-/issues/11))

## [3.7.12] - 2023-11-27

### Added

-   new waste collection router ([p0149#310](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/310))

## [3.7.11] - 2023-11-22

### Changed

-   tests for Parkings module

## [3.7.10] - 2023-11-15

### Changed

-   Removing schema definitions ([schema-definitions#55](https://gitlab.com/operator-ict/golemio/code/modules/schema-definitions/-/issues/55))

## [3.7.9] - 2023-11-13

-   No changelog

## [3.7.8] - 2023-10-25

-   No changelog

## [3.7.7] - 2023-10-18

-   No changelog

## [3.7.6] - 2023-10-09

### Changed

-   CI/CD - Update Redis services to v7.2.1
-   Update apiary/openapi docs, change swagger-ui config

## [3.7.5] - 2023-09-27

### Added

-   API versioning - abstract router loader ([core#80](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/80))

## [3.7.4] - 2023-09-13

-   No changelog

## [3.7.3] - 2023-09-06

-   No changelog

## [3.7.2] - 2023-09-04

-   No changelog

## [3.7.1] - 2023-08-28

-   No changelog

## [3.7.0] - 2023-08-23

### Changed

-   Generate OAS file via Golemio CLI ([core#46](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/46))

## [3.6.11] - 2023-08-16

-   No changelog

## [3.6.10] - 2023-08-09

-   No changelog

## [3.6.9] - 2023-08-07

-   No changelog

## [3.6.8] - 2023-08-02

-   No changelog

## [3.6.7] - 2023-07-31

### Changed

-   NodeJS 18.17.0

## [3.6.6] - 2023-07-24

-   No changelog

## [3.6.5] - 2023-07-18

-   hotfix for vehicle sharing after upgrade to postgres 14

## [3.6.4] - 2023-07-17

### Fixed

-   Opentelemetry initialization

## [3.6.3] - 2023-07-12

-   No changelog

## [3.6.2] - 2023-07-10

### Changed

-   Replaced Shared-Bikes with Vehiclesharing ([issue](https://gitlab.com/operator-ict/golemio/code/modules/shared-bikes/-/issues/15))

## [3.6.1] - 2023-06-26

### Fixed

-   Openapi config parkings

## [3.6.0] - 2023-06-26

### Changed

-   Refactoring connectors ([core#17](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/17))

## [3.5.9] - 2023-06-21

-   No changelog

## [3.5.8] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [3.5.7] - 2023-06-07

-   No changelog

## [3.5.6] - 2023-06-05

### Changed

-   Update apiary/openapi docs
-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))

## [3.5.5] - 2023-05-31

### Changed

-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))

## [3.5.4] - 2023-05-30

-   No changelog

## [3.5.3] - 2023-05-17

-   No changelog

## [3.5.2] - 2023-05-15

### Fixed

-   memory leak in Cache middleware

## [3.5.1] - 2023-04-26

### Removed

-   Remove Mongo ([core#66](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/66))

## [3.5.0] - 2023-04-12

### Changed

-   removed modules: bicycle-parkings, parking-zones, public-toilets, shared-cars, meteosensors, traffic-cameras
-   removed general-routes

## [3.4.7] - 2023-04-03

-   No changelog

## [3.4.6] - 2023-03-29

### Added

-   Inspect utils for remote debugging ([output-gateway#219](https://gitlab.com/operator-ict/golemio/code/output-gateway/-/issues/219))

## [3.4.5] - 2023-03-27

### Added

-   Add shared cars router as a replacement of deprecated shared-cars module ([shared-bikes#14](https://gitlab.com/operator-ict/golemio/code/modules/shared-bikes/-/issues/14))

## [3.4.4] - 2023-03-22

-   No changelog

## [3.4.3] - 2023-03-15

-   No changelog

## [3.4.2] - 2023-03-13

### Added

-   Microclimate API ([microclimate#2](https://gitlab.com/operator-ict/golemio/code/modules/microclimate/-/issues/2))

## [3.4.1] - 2023-03-06

### Removed

-   Removed Sorted Waste Stations module ([sorted-waste-stations/#17](https://gitlab.com/operator-ict/golemio/code/modules/sorted-waste-stations/-/issues/17))

## [3.4.0] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

### Fixed

-   Update apiary and openapi docs

## [3.3.1] - 2023-02-22

-   No changelog

## [3.3.0] - 2023-02-13

-   No changelog

## [3.2.2] - 2023-02-08

-   No changelog

## [3.2.1] - 2023-02-07

-   No changelog

## [3.2.0] - 2023-02-06

-   No changelog

## [3.1.1] - 2022-02-01

-   No changelog

## [3.1.0] - 2023-01-23

### Changed

-   Docker image optimization
-   Migrate to npm

## [3.0.45] - 2023-01-04

### Added

-   swagger docs blacklist

## [3.0.44] - 2023-01-04

### Changed

-   update golemio/core with dependency injection

## [3.0.43] - 2022-21-07

-   No changelog

## [3.0.42] - 2022-11-29

### Changed

-   Stop serving `/.well-known/security.txt`

### Removed

-   Remove Express serve-static middleware ([vp/og#1](https://gitlab.com/operator-ict/golemio/code/vp/output-gateway/-/issues/1))

## [3.0.41] - 2022-11-14

### Added

-   Added warning for migration to Swagger UI

### Removed

-   Removed PID documentation and replaced by link to Swagger UI

## [3.0.40] - 2022-11-03

### Changed

-   Update TypeScript to v4.7.2

## [3.0.39] - 2022-10-11

-   No changelog

## [3.0.38] - 2022-09-21

### Changed

-   Update Node.js to 16.17.0 ([code/general#418](https://gitlab.com/operator-ict/golemio/code/general/-/issues/418)

### Removed

-   Unused dependencies ([modules/general#5](https://gitlab.com/operator-ict/golemio/code/modules/general/-/issues/5))

## [3.0.37] - 2022-09-06

-   no changelog

## [3.0.36] - 2022-08-24

### Removed

-   removed shared bikes from Apiary [#9](https://gitlab.com/operator-ict/golemio/code/modules/shared-bikes/-/issues/9)

## [3.0.32] - 2022-07-13

-   no changelog

## [3.0.31] - 2022-06-21

-   no changelog

## [3.0.30] - 2022-06-07

-   no changelog

## [3.0.26] - 2022-05-02

### Added

-   openapi: 3.0.3 docs

