import { BaseApp, IServiceCheck, Service, getServiceHealth } from "@golemio/core/dist/helpers";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { IRedisConnector } from "@golemio/core/dist/helpers/data-access/redis/IRedisConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { initSentry, metricsService } from "@golemio/core/dist/monitoring";
import { log, requestLogger } from "@golemio/core/dist/output-gateway/Logger";
import { config } from "@golemio/core/dist/output-gateway/config/config";
import { ContainerToken, OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import {
    ErrorHandler,
    FatalError,
    GeneralError,
    HTTPErrorHandler,
    IGolemioError,
} from "@golemio/core/dist/shared/golemio-errors";
import { Lightship, createLightship } from "@golemio/core/dist/shared/lightship";
import sentry from "@golemio/core/dist/shared/sentry";
import compression from "compression";
import http from "http";
import swaggerUi, { SwaggerOptions } from "swagger-ui-express";
import { RouterLoader } from "./RouterLoader";
import {
    airQualityRouter,
    bicycleCountersRouter,
    cityDistrictsRouter,
    fcdRouter,
    gardensRouter,
    gbfsRouter,
    medicalInstitutionsRouter,
    microclimateRouter,
    municipalAuthoritiesRouter,
    municipalLibrariesRouter,
    municipalPoliceStationsRouter,
    pedestriansRouter,
    playgroundsRouter,
    sharedCarsRouter,
    trafficRouter,
    vehiclesharingRouter,
    wasteCollectionLegacyRouter,
    wasteCollectionYardsRouter,
} from "./routers";

/**
 * Entry point of the application. Creates and configures an ExpressJS web server.
 */
export default class App extends BaseApp {
    public express: express.Application = express();
    public port: number = parseInt(config.port || "3004", 10);
    private server?: http.Server;
    public metricsServer?: http.Server;
    private commitSHA!: string;
    private lightship: Lightship;
    private readonly simpleConfig: ISimpleConfig;
    private readonly postgresConnector: IDatabaseConnector;
    private readonly redisConnector: IRedisConnector;

    /**
     * Run configuration methods on the Express instance
     */
    constructor() {
        super();
        this.lightship = createLightship({
            detectKubernetes: config.node_env !== "production",
            shutdownHandlerTimeout: config.lightship.handlerTimeout,
            gracefulShutdownTimeout: config.lightship.shutdownTimeout,
            shutdownDelay: config.lightship.shutdownDelay,
        });
        process.on("uncaughtException", (err: Error) => {
            log.error(err);
            this.lightship.shutdown();
        });
        process.on("unhandledRejection", (reason: any) => {
            log.error(reason);
            this.lightship.shutdown();
        });
        this.simpleConfig = OutputGatewayContainer.resolve<ISimpleConfig>(CoreToken.SimpleConfig);
        this.postgresConnector = OutputGatewayContainer.resolve<IDatabaseConnector>(ContainerToken.PostgresDatabase);
        this.redisConnector = OutputGatewayContainer.resolve<IRedisConnector>(ContainerToken.RedisConnector);
    }

    /**
     * Start the application and runs the server
     */
    public start = async (): Promise<void> => {
        try {
            this.express = express();
            initSentry(config.sentry, config.app_name, this.express);
            metricsService.init(config, log);
            this.commitSHA = this.loadCommitSHA();
            log.info(`Commit SHA: ${this.commitSHA}`);
            await this.database();
            this.middleware();
            await this.routes();
            this.errorHandlers();
            this.metricsServer = metricsService.serveMetrics();
            this.server = http.createServer(this.express);
            // Setup error handler hook on server error
            this.server.on("error", (err: Error) => {
                sentry.captureException(err);
                ErrorHandler.handle(new FatalError("Could not start a server", "App", err), log);
            });
            // Serve the application at the given port
            this.server.listen(this.port, () => {
                // Success callback
                log.info(`Listening at http://localhost:${this.port}/`);
            });
            this.lightship.registerShutdownHandler(async () => {
                await this.gracefulShutdown();
            });
            this.lightship.signalReady();
        } catch (err) {
            sentry.captureException(err);
            ErrorHandler.handle(err, log);
        }
    };

    /**
     * Graceful shutdown - terminate connections and server
     */
    private gracefulShutdown = async (): Promise<void> => {
        log.info("Graceful shutdown initiated.");
        await this.stop();
        await this.metricsServer?.close();
        OutputGatewayContainer.dispose();
    };

    public stop = async (): Promise<void> => {
        this.server?.close();
    };

    private database = async (): Promise<void> => {
        await this.postgresConnector.connect();

        if (this.simpleConfig.getBoolean("env.REDIS_ENABLE", false)) {
            await this.redisConnector.connect();
        }
    };

    /**
     * Set custom headers
     */
    private customHeaders = (_req: Request, res: Response, next: NextFunction): void => {
        res.setHeader("Access-Control-Allow-Methods", "GET, OPTIONS, HEAD");
        next();
    };

    /**
     * Bind middleware to express server
     */
    private middleware = (): void => {
        this.express.use((req, res, next) => {
            const beacon = this.lightship.createBeacon();
            res.on("finish", () => {
                beacon.die();
            });
            next();
        });

        // TODO: Sentry middleware cause memory issues related to
        //   https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/90
        // this.express.use(sentry.Handlers.requestHandler() as express.RequestHandler);
        // this.express.use(sentry.Handlers.tracingHandler() as express.RequestHandler);

        this.express.use(metricsService.metricsMiddleware());
        this.express.use(requestLogger);
        this.express.use(this.commonHeaders);
        this.express.use(this.customHeaders);
        this.express.use(compression());
    };

    private healthCheck = async () => {
        const description = {
            app: "Golemio Data Platform Output Gateway",
            commitSha: this.commitSHA,
            version: config.app_version,
        };

        const services: IServiceCheck[] = [
            {
                name: Service.POSTGRES,
                check: () => this.postgresConnector.isConnected(),
            },
        ];

        if (this.simpleConfig.getBoolean("env.REDIS_ENABLE", false)) {
            services.push({ name: Service.REDIS, check: () => Promise.resolve(this.redisConnector.isConnected()) });
        }

        const serviceStats = await getServiceHealth(services);
        return { ...description, ...serviceStats };
    };

    /**
     * Define express server routes
     */
    private routes = async (): Promise<void> => {
        const defaultRouter: express.Router = express.Router();

        // Create base url route handler
        defaultRouter.get(
            ["/", "/health-check", "/status"],
            async (req: express.Request, res: express.Response, next: express.NextFunction) => {
                try {
                    const healthStats = await this.healthCheck();
                    if (healthStats.health) {
                        return res.json(healthStats);
                    } else {
                        return res.status(503).send(healthStats);
                    }
                } catch (err) {
                    return res.status(503);
                }
            }
        );

        // Create specific routes with their own router
        this.express.use("/", defaultRouter);
        this.express.use("/airqualitystations", airQualityRouter);
        this.express.use("/bicyclecounters", bicycleCountersRouter);
        this.express.use("/citydistricts", cityDistrictsRouter);
        this.express.use("/medicalinstitutions", medicalInstitutionsRouter);
        this.express.use("/municipalauthorities", municipalAuthoritiesRouter);
        this.express.use("/municipallibraries", municipalLibrariesRouter);
        this.express.use("/gardens", gardensRouter);
        this.express.use("/sortedwastestations", wasteCollectionLegacyRouter);
        this.express.use("/wastecollectionyards", wasteCollectionYardsRouter);
        this.express.use("/playgrounds", playgroundsRouter);
        this.express.use("/sharedbikes", vehiclesharingRouter);
        this.express.use("/sharedbikes/gbfs", gbfsRouter);
        this.express.use("/sharedcars", sharedCarsRouter);
        this.express.use("/pedestrians", pedestriansRouter);
        this.express.use("/traffic", trafficRouter);
        this.express.use("/fcd", fcdRouter);
        this.express.use("/municipalpolicestations", municipalPoliceStationsRouter);
        this.express.use("/microclimate", microclimateRouter);
        this.express.use("/vehiclesharing", vehiclesharingRouter);
        this.express.use("/vehiclesharing/gbfs", gbfsRouter);

        const routerLoader = new RouterLoader(["energetics", "parkings", "vehiclesharing", "waste-collection", "waze-ccp"]);
        await routerLoader.registerRouters(this.express);

        // ApiDocs
        let swaggerOptions: SwaggerOptions = {
            docExpansion: "none", // expands nothing
        };

        const oasFileUrl = "/docs/static/output-gateway/openapi.json";
        this.express.use(oasFileUrl, express.static("docs/generated/openapi.json"));
        swaggerOptions.url = oasFileUrl;
        this.express.use(
            "/docs/openapi",
            swaggerUi.serveFiles(undefined, { swaggerOptions }),
            swaggerUi.setup(undefined, {
                swaggerOptions,
                customSiteTitle: "Golemio API Documentation",
                customJs: "/docs/js/plausible.js",
            })
        );

        const oasPublicFileUrl = "/docs/static/output-gateway/openapi-public.json";
        this.express.use(oasPublicFileUrl, express.static("docs/generated/openapi-public.json"));
        swaggerOptions.url = oasPublicFileUrl;
        this.express.use(
            "/docs/public-openapi",
            swaggerUi.serveFiles(undefined, { swaggerOptions }),
            swaggerUi.setup(undefined, {
                swaggerOptions,
                customSiteTitle: "Open Data Golemio API Documentation",
                customJs: "/docs/js/plausible.js",
            })
        );

        // Plausible analytics for API documentation
        this.express.use("/docs/js/plausible.js", express.static("public/plausible.js"));
    };

    /**
     * Define error handling middleware
     */
    private errorHandlers = (): void => {
        this.express.use(sentry.Handlers.errorHandler({ shouldHandleError: () => true }) as express.ErrorRequestHandler);

        // Request aborted error
        this.express.use((err: any, req: Request, _res: Response, next: NextFunction) => {
            if (err.type === "request.aborted") {
                next(new GeneralError("Request aborted", "App", new Error(`Called ${req.method} ${req.url}`), 400));
            } else {
                next(err);
            }
        });

        // Not Acceptable error
        this.express.use((err: any, req: Request, _res: Response, next: NextFunction) => {
            if (err.status === 406) {
                next(
                    new GeneralError(
                        "Not Acceptable",
                        "App",
                        new Error(`Header ${req.get("Accept")} is not acceptable for ${req.method} ${req.url}`),
                        406
                    )
                );
            } else {
                next(err);
            }
        });

        // Not found error - no route was matched
        this.express.use((req, _res, next) => {
            next(new GeneralError("Route not found", "App", new Error(`Called ${req.method} ${req.url}`), 404));
        });

        // Error handler to catch all errors sent by routers (propagated through next(err))
        this.express.use((err: any, _req: Request, res: Response, _next: NextFunction) => {
            const warnCodes = [400, 404, 406];
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log, warnCodes.includes(err.status) ? "warn" : "error");
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    };
}
