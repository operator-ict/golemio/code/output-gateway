import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import express from "express";
import { FatalError } from "@golemio/errors";
import { AbstractRouter } from "@golemio/core/dist/helpers/routing/AbstractRouter";
import { RouterLoader } from "../src/RouterLoader";

chai.use(chaiAsPromised);

describe("RouterLoader", () => {
    describe("loadRouters", () => {
        it("should load routers", async () => {
            const routerLoader = new RouterLoader(["parkings"]);

            const result = await routerLoader["loadRouters"]();

            expect(Array.isArray(result)).to.be.true;
            expect(result).to.have.length(7);
            expect(result.filter((router) => router instanceof AbstractRouter)).to.have.length(7);
        });

        it("should reject (non-existent module)", async () => {
            const routerLoader = new RouterLoader(["non-existing"]);

            await expect(routerLoader["loadRouters"]()).to.be.rejectedWith(
                FatalError,
                "Cannot import router for module: @golemio/non-existing/dist/output-gateway."
            );
        });
    });

    describe("registerRouters", () => {
        it("should register routers", async () => {
            const app = express();
            const routerLoader = new RouterLoader(["parkings"]);

            await routerLoader.registerRouters(app);

            const routes = app._router.stack.filter((r: any) => r.name === "router"); // get all the paths
            expect(routes).to.have.length(7);
        });
    });
});
